import React, {useState, useEffect} from "react"
import Section from "./components/Section"
import Form from "./components/Form"
import Seconds from "./components/Seconds"
import List from "./components/List"
import todoApi from "./apis"
//import Test from "./components/Test"
//import Search from "./components/Search"
import MyIp from "./components/Myip"

const appTittle = 'To do App'

const App = () => {
    const [toDoList, setToDoList] = useState([])
    useEffect( () => {
        async function getData () {
            const { data } = await todoApi.get("/todos")
            setToDoList(data)
        }
        getData();
    },[])

    const addTodo = async (toDo) => {
        const { data } = await todoApi.post("/todos", toDo)
        setToDoList((oldlist) => [...oldlist, data])
    }

    const removeToDo = async (id) => {
        await todoApi.delete(`/todos/${id}`)
        setToDoList((oldlist) => oldlist.filter((item) => item._id !== id ))
    }
    const editToDo = async (id, item) => {
        await todoApi.put(`/todos/${id}`, item)
    }

    return (
        <div className="ui container center aligned">
            <Section>
                    <h1>{appTittle}</h1>
            </Section>
            <Section>
                <Form addTodo={addTodo} />
            </Section>
            <Section>
                <List 
                    toDoList={toDoList} 
                    removeToDoList={removeToDo} 
                    editToDoList={editToDo}
                />
            </Section>
            <Section>
                <Seconds />
                <MyIp />
            </Section>
        </div>
    )
}

export default App;