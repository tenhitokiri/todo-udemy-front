import React, { useState } from "react"

const Form = ({addTodo}) => {
    const [inputValue, setInputValue] = useState("")
    const handleInputChange = (e) => {
        setInputValue(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if (inputValue.trim()) {
            addTodo({ tittle: inputValue, done: false})
        }
        setInputValue("")
    }

    return(
        <form className="ui form" onSubmit={handleSubmit}>
            <div className="ui grid center aligned">
                <div className="row">
                    <div className="column five wide">
                        <input 
                            value={inputValue}
                            onChange={handleInputChange}
                            type="text" 
                            placeholder="Enter some sheet"
                        />
                    </div>
                    <div className="column one wide">
                        <button type="submit" className="ui button circular icon green"><i className="plus icon white"></i></button>
                    </div>
                </div>
            </div>
        </form>
    )
}
/* export default class Form extends React.Component{
    constructor(){
        super();
        this.state = { value: "write your name"}
    }
    handleFormSubmit = (event) => {
        event.preventDefault();
        alert(this.state.value);
    }
    handleInputChange = (event) => {
        this.setState({ value: event.target.value })
    }   
    render(){
        return (
        <form onSubmit={this.handleFormSubmit}>
            <input id="name" type="text" value={this.state.value.toUpperCase()} onChange={(event) => this.handleInputChange(event)} />
            <input type="submit" />
        </form>
            )}
} */

export default Form;