import React, {useState} from "react"

const Counter = ({name}) => {
    const [clicks, setClicks] = useState(0)
    const handleClick = () => {
        setClicks(clicks+1)
    }
    return(
        <>
        <span>Hi {name} You clicked {clicks} times </span>
        <br/>
        <button onClick={handleClick} >Click Me!</button>
        </>
    )}

/* export class Counter extends React.Component{
    constructor(){
        super();
        this.state = {counter : 0}
        this.timer = null;
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.setState({
                counter: this.state.counter+1
            })
        }, 1000
            )
    }
    
    componentWillMount(){
        clearInterval(this.timer)
    }

    render() {
        return <span>{this.state.counter}</span>
    }
}
 */

export default Counter;