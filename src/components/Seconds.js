import React, {useEffect, useState} from "react"

const Seconds = () => {
    const [counter, setCounter] = useState(0)
    useEffect(() => {
        const timer = setInterval(() =>{
            setCounter(counter => counter+1)
        }, 1000);

        return () => {
            clearInterval(timer);
        }
    }, [])
/*     useEffect(() => {
        console.log("every time");
        return () => console.log("Called before unmounting");
    }, [])
 */    return (
        <div className="ui segment">
            <span>You've spent {counter} seconds on this page!</span>
        </div>
    )
}

export default Seconds;