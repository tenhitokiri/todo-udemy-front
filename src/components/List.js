import React from "react"
import Todo from "./Todo"

const List = ({ toDoList, removeToDoList, editToDoList }) => {
    const renderedList = toDoList.map(
        (toDo) => (
                <Todo title={toDo.tittle} 
                    completed={toDo.completed} 
                    removeToDoItem={(e)=> removeToDoList(toDo._id) } 
                    editToDoItem={(updatedToDo)=> editToDoList(toDo._id, updatedToDo) } 
                    key={toDo._id}
                />
            )
        )
    //console.table(toDoList);
    return (
        <div className="ui grid center aligned">
            {renderedList}
        </div>
    )
    }

export default List