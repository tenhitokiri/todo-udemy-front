import React , {useState} from "react"

const Todo = ({ title, completed, removeToDoItem, editToDoItem}) => {
    const [isEditing, setIsEditing] = useState(false)
    const [toDoValue, setToDoValue] = useState(title)
    const [tmpToDoValue, setTmpToDoValue] = useState(title)
    const [done, setDone] =useState(completed)

    const handleDivDoubleClick = () => {
        setIsEditing(!isEditing)
    }

    const handleComplete = () => {
        setDone((comp) => !comp)
        editToDoItem({completed: !done})
    }
    const handleInputKeyDown = (e) => {
        const pressedKey = e.keyCode

        if (pressedKey === 13 || pressedKey === 27) {
            if (pressedKey === 13) {
                setToDoValue(tmpToDoValue)
                editToDoItem({tittle: tmpToDoValue})
            }
            else {
                setTmpToDoValue(toDoValue)
            }
            setIsEditing(false)
        }
    }

    const handleInputChange = (e) => {
        setTmpToDoValue(e.target.value)
    }

    return ( isEditing ?
            <div className="row" onDoubleClick={handleDivDoubleClick}>
                <div className="column seven wide">
                    <div className="ui input fluid">
                        <input 
                            onKeyDown={handleInputKeyDown}
                            autoFocus={true}
                            value={tmpToDoValue}
                            onChange={handleInputChange}
                        /> 
                    </div>
                </div>
            </div>
            :
            <div className="row" >
                <div className="column five wide" onDoubleClick={handleDivDoubleClick}>
                    <h3 className={"ui header" + (done ? " green" : "")}>
                    {toDoValue}                     </h3>
                </div>
                <div className="column one wide">
                <button
                    type="submit" 
                    className={"ui button circular icon "+ (done ? " blue" : "green")}
                    onClick={handleComplete}
                >
                    <i className="check icon white" />
                </button>
                </div>
                <div className="column one wide">
                <button 
                    type="submit" 
                    className="ui button circular icon red"
                    onClick={removeToDoItem}
                ><i className="remove icon white"></i></button>
                </div>
            </div>
    )
}

export default Todo