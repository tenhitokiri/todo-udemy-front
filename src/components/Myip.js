import React , { useState, useEffect} from "react"
import axios from "axios"

const MyIp = () => {
    const [ip, setIp] = useState(null)
    
    useEffect(() => {

        async function getIp () {
            const  {data} = await axios.get("http://45.181.250.100:8012")
            setIp(data.clientIp)
        }
        getIp();

    }, [])
    return (
        <div className="ui segment">
            <span>nos visitas desde la ip Pública {ip} </span>
        </div>
    )
}

export default MyIp